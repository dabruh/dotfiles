#!/bin/sh

xrandr --output DVI-D-0 --off --output HDMI-0 --mode 1920x1200 --pos 0x0 --rotate left --output HDMI-1 --mode 1920x1080 --pos 1520x1565 --rotate normal --output DP-0 --off --output DP-1 --mode 1920x1200 --pos 3760x0 --rotate right --output DP-2 --mode 2560x1440 --pos 1200x125 --rotate normal --output DP-3 --off
