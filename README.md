# dabruh's dotfiles

## Showcase

- [3ximus's aerial theme for SDDM](https://github.com/3ximus/aerial-sddm-theme/)

- i3 with picom compositor and rounded corners

    ![screenshot](./src/screenshot.png)

- Layered i3/i3status configuration:

    ```sh
    ~ >>> hostname
    abdiel

    ~ >>> tree ~/.config/i3
    ~/.config/i3
    ├── config
    └── layers
        ├── abdiel.layer
        ├── config.main
        ├── config.post-main-00-security
        ├── config.post-main-01-visuals
        ├── config.post-main-10-applets
        ├── config.post-main-10-audio
        ├── config.post-main-10-misc
        ├── config.post-main-90-gaps
        ├── config.post-main-90-laptop
        ├── config.pre-main-00-vars
        ├── config.pre-main-90-abdiel
        └── default.layer

    ~ >>> cat ~/.config/i3/layers/default.layer ~/.config/i3/layers/abdiel.layer | sort -u
    config.main
    config.post-main-00-security
    config.post-main-01-visuals
    config.post-main-10-applets
    config.post-main-10-audio
    config.post-main-10-misc
    config.post-main-90-gaps
    config.pre-main-00-vars
    config.pre-main-90-abdiel

    ~ >>> i3-config-builder
    Building i3 config... 🔨
    WRITE : 'config.pre-main-00-vars'.
    WRITE : 'config.pre-main-90-abdiel'.
    WRITE : 'config.main'.
    WRITE : 'config.post-main-00-security'.
    WRITE : 'config.post-main-01-visuals'.
    WRITE : 'config.post-main-10-applets'.
    WRITE : 'config.post-main-10-audio'.
    WRITE : 'config.post-main-10-misc'.
    WRITE : 'config.post-main-90-gaps'.

    Building i3status config... 🔨
    WRITE : 'config.main'.
    WRITE : 'config.post-main-01-cmus'.
    WRITE : 'config.post-main-01-docker'.
    WRITE : 'config.post-main-25'.
    WRITE : 'config.post-main-50-abdiel'.
    WRITE : 'config.post-main-75'.

    Restarting i3... 🔄
    2022-05-29 00:38:27 - Additional arguments passed. Sending them as a command to i3.
    [{"success":true}]

    ~ >>>
    ```

## Giving credit where credit is due

The following list of scripts are either full copies, or modifications of the below sources. You may find a few scripts in this repo that are not mentioned below. They are not mentioned for one of the following 3 reasons:

- They're developed by me.
- They're too small/generic.
- They've been too heavily modified to resemble to original.

| path                                | source                                                                                                                                                                                                            |
| ----------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `.config/fontconfig/fonts.conf`     | [LukeSmithxyz/voidrice](https://github.com/LukeSmithxyz/voidrice/blob/d283f12ef9c524a88550097bceda029b3eb9a71c/.config/fontconfig/fonts.conf)                                                                                                                                                                                         |
| `.config/picom/picom.conf`          | [gaurav712/picom.conf](https://gist.github.com/gaurav712/42549678b778db12fda5764f7c0757ad)                                                                                                                        |
| `.local/bin/dmenumount`             | [LukeSmithxyz/voidrice](https://github.com/LukeSmithxyz/voidrice/blob/d283f12ef9c524a88550097bceda029b3eb9a71c/.local/bin/dmenumount)                                                                             |
| `.local/bin/dmenuumount`            | [LukeSmithxyz/voidrice](https://github.com/LukeSmithxyz/voidrice/blob/d283f12ef9c524a88550097bceda029b3eb9a71c/.local/bin/dmenuumount)                                                                            |
| `.local/bin/dmenuunicode`           | [LukeSmithxyz/voidrice](https://github.com/LukeSmithxyz/voidrice/blob/d283f12ef9c524a88550097bceda029b3eb9a71c/.local/bin/dmenuunicode)                                                                           |
| `.local/bin/i3-get-window-criteria` | [faq.i3wm.org/question/2172](https://faq.i3wm.org/question/2172/how-do-i-find-the-criteria-for-use-with-i3-config-commands-like-for_window-eg-to-force-splashscreens-and-dialogs-to-show-in-floating-mode.1.html) |
| `.local/share/larbs/emoji`          | [LukeSmithxyz/voidrice](https://github.com/LukeSmithxyz/voidrice/blob/d283f12ef9c524a88550097bceda029b3eb9a71c/.local/share/larbs/emoji)                                                                                                                                                                                         |
